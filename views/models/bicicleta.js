var bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
};
bicicleta.prototype.toString = function () {
  return "id" + this.id + " | color: " + this.color;
};

bicicleta.allBicis = [];
bicicleta.add = function (aBici) {
  bicicleta.allBicis.push(aBici);
};

var a = new bicicleta(1, "rojo", "urbana", [-34.943084, -54.925835]);
var b = new bicicleta(2, "blanca", "rutera", [-34.938456, -54.944989]);

bicicleta.add(a);
bicicleta.add(b);

module.exports = bicicleta;
